const static_files = require('rollup-plugin-static-files')
const typescript = require('@rollup/plugin-typescript')
const serve = require('rollup-plugin-serve')

module.exports = {
  input: "src/main.js",
  output: {
    file: "build/bundle.js",
  },
  plugins: [
    static_files({
      include: ['./public'],
    }),
    typescript(),
    serve('build'),
  ]
}

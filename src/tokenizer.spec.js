import Tokenizer from './tokenizer'


describe('something', () => {
  it('should tokenize basic statement', () => {
    const got = Tokenizer.tokenize('select * from x')
    const output = [
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: '*', type: 'star'},
      {value: ' ', type: 'whitespace'},
      {value: 'from', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'x', type: 'name'},
      {value: '', type: 'EOF'},
    ]
    expect(got).toStrictEqual(output)
  })

  it('should treat multiple whitespaces as one', () => {
    const got = Tokenizer.tokenize('select  *')
    const output = [
      {value: 'select', type: 'keyword'},
      {value: '  ', type: 'whitespace'},
      {value: '*', type: 'star'},
      {value: '', type: 'EOF'},
    ]
    expect(got).toStrictEqual(output)
  })

  it('should treat tabs as whitespaces', () => {
    const got= Tokenizer.tokenize('update\t1')
    const output = [
      {value: 'update', type: 'keyword'},
      {value: '\t', type: 'whitespace'},
      {value: '1', type: 'number'},
      {value: '', type: 'EOF'},
    ]
    expect(got).toStrictEqual(output)
  })

  it('should parse trailing whitespaces', () => {
    const got = Tokenizer.tokenize(' select ')
    const output = [
      {value: ' ', type: 'whitespace'},
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: '', type: 'EOF'},
    ]

    expect(got).toStrictEqual(output)
  })

  it('should separate tokens with commas', () => {
    const got = Tokenizer.tokenize('select a,b from x')
    const output = [
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'a', type: 'name'},
      {value: ',', type: 'comma'},
      {value: 'b', type: 'name'},
      {value: ' ', type: 'whitespace'},
      {value: 'from', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'x', type: 'name'},
      {value: '', type: 'EOF'},
    ]
    expect(got).toStrictEqual(output)
  })

  it('should separate brackets from tokens', () => {
    const got = Tokenizer.tokenize('update m (id) values (1)')
    const output = [
      {value: 'update', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'm', type: 'name'},
      {value: ' ', type: 'whitespace'},
      {value: '(', type: 'openb'},
      {value: 'id', type: 'name'},
      {value: ')', type: 'closeb'},
      {value: ' ', type: 'whitespace'},
      {value: 'values', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: '(', type: 'openb'},
      {value: '1', type: 'number'},
      {value: ')', type: 'closeb'},
      {value: '', type: 'EOF'},
    ]

    expect(got).toStrictEqual(output)
  })

  it('should separate dots', () => {
    const got = Tokenizer.tokenize('select a.b')
    const output = [
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'a', type: 'name'},
      {value: '.', type: 'dot'},
      {value: 'b', type: 'name'},
      {value: '', type: 'EOF'},
    ]

    expect(got).toStrictEqual(output)
  })

  it('should separate statements', () => {
    const got = Tokenizer.tokenize('a;b')
    const output = [
      {value: 'a', type: 'name'},
      {value: ';', type: 'colon'},
      {value: 'b', type: 'name'},
      {value: '', type: 'EOF'},
    ]

    expect(got).toStrictEqual(output)
  })

  it('should aprse as', () => {
    const got = Tokenizer.tokenize('a as b')
    const output = [
      {value: 'a', type: 'name'},
      {value: ' ', type: 'whitespace'},
      {value: 'as', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'b', type: 'name'},
      {value: '', type: 'EOF'},
    ]
    expect(got).toStrictEqual(output)
  })

})

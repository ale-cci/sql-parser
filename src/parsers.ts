interface Token {
    type: string,
    value: string,
}

interface TokenRule {
    type: string,
    value: RegExp | undefined,
}

interface ParserMap {
    [parserName: string]: Parser
}

interface Parser {
    (parserMap: ParserMap, tokens: Token[]): number,
    optional?: boolean,
}


function MAll(parserList: Array<Parser>): Parser {
    return (map, tokens) => {
        let parsedAmount = 0

        for (const p of parserList) {
            let parsedNow = p(map, tokens.slice(parsedAmount))
            parsedAmount += parsedNow

            if (parsedNow === 0 && !p.optional) {
                return 0
            }
        }
        return parsedAmount
    }
}


function MRule(ruleName: string): Parser {
    return (map, tokens) => {
        return map[ruleName](map, tokens)
    }
}


function MOneOf(parserList: Array<Parser>): Parser {
    return (map, tokens) =>
        Math.max(...parserList.map(p => p(map, tokens)))
}


function MToken(token: Token): Parser {
    return (_map, tokens) => {
        if (!tokens.length) {
            return 0;
        }
        const fst = tokens[0]
        if (token.value !== undefined && token.value !== fst.value) {
            return 0
        }
        return (token.type === fst.type) ? 1 : 0
    }
}


function MAny(p: Parser): Parser {
    return (m, tokens) => {
        let parsed = 0
        while (true) {
            const parsedNow = p(m, tokens.slice(parsed))

            if (parsedNow === 0) {
                break
            }
            parsed += parsedNow
        }
        return parsed
    }
}

function Opt(parser: Parser): Parser {
    parser.optional = true
    return parser
}

function MKey(value): Parser {
    return MToken({type: 'keyword', value})
}

export {MAll, MRule, MOneOf, MToken, MAny, Opt, MKey}

import {MAll, MRule, MOneOf, MToken, MAny, Opt, MKey} from './parsers'

const parsingRules = {
  'file': MAll([
    Opt(MAny(
      MAll([
        Opt(MRule('stmt')),
        Opt(MToken({value: ';', type: 'colon'})),
      ])
    )),
    MToken({value: '', type: 'EOF'}),
  ]),

  'stmt': MAll([
    Opt(MOneOf([
      MRule('select'),
    ])),
  ]),

  'select': MAll([
    MKey('select'),
    Opt(MOneOf([
      MKey('distinct'),
      MKey('all'),
    ])),
    MRule('select-item'),
    Opt(MAny(MAll([
      MToken({value: ',', type: 'comma'}),
      MRule('select-item'),
    ]))),
    Opt(MAll([
      MKey('from'),
      MToken({type: 'name'}),
    ])),
    Opt(MAll([
      MKey('where'),
    ])),
  ]),

  'update': MKey('update'),

  'select-item': MOneOf([
    MToken({type: 'name'}),
    MToken({type: 'star', value: '*'}),
    MAll([
      MToken({type: 'name'}),
      MKey('as'),
      MToken({type: 'name'}),
    ]),
    MAll([
      MToken({type: 'name'}),
      MToken({type: 'dot'}),
      MOneOf([MToken({type: 'name'}), MToken({type: 'star'})]),
    ]),
    MToken({type: 'number'}),
    MAll([
      MToken({type: 'openb'}),
      MRule('select'),
      MToken({type: 'closeb'}),
      Opt(MAll([MKey('as'), MToken({type: 'name'})])),
    ])
  ])
}


export {parsingRules}

import {makePeeker} from './peeker'
import {keywords} from './keywords'

String.prototype.isWhiteSpace = function() {
  return [...this].every(c => [' ', '\t'].indexOf(c) !== -1)
}



function readUntil(peeker, condition) {
  let tok = ''
  while (peeker.peek() !== undefined && condition(peeker.peek())) {
    tok += peeker.pop()
  }
  return tok
}

/**
  * SQL - Tokenizer
  */
function tokenize(stmt) {
  const peeker = makePeeker(stmt)
  const reader = readUntil.bind(null, peeker)

  let tok = '', tokens = []

  const specialTokens = {
    '.': 'dot',
    ';': 'colon',
    ',': 'comma',
    '(': 'openb',
    ')': 'closeb',
    '*': 'star',
  }

  for (let l; (l = peeker.peek()) !== undefined;) {
    if (l.isWhiteSpace()) {
      tok = reader(c => c.isWhiteSpace())
      tokens.push({value: tok, type: 'whitespace'})
    } else if (l in specialTokens) {
      peeker.pop()
      tokens.push({value: l, type: specialTokens[l]})
    } else {
      tok = reader(c => (!c.isWhiteSpace() && !(c in specialTokens)))
      let type
      if (keywords.indexOf(tok.toUpperCase()) !== -1) {
        type = 'keyword'
      } else if (tok.split('').every(c => ('0' <= c) && (c <= '9'))) {
        type = 'number'
      } else {
        type = 'name'
      }

      tokens.push({value: tok, type})
    }
  }

  tokens.push({value: '', type: 'EOF'})
  return tokens
}

export default {tokenize}

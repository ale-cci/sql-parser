
function makePeeker(iterable) {
  let i = 0
  return {
    pop: () => iterable[i++],
    peek: (n=0) => iterable[i+n],
  }
}

export {
  makePeeker
}

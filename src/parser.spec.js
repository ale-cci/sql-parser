import Parser from './parser'

describe('parser', () => {
  it('should parse an empty statement', () => {
    const got = Parser.parse([
      {value: '', type: 'EOF'},
    ])
    const output = ''

    expect(got).toBe(output)
  })

  it('should parse a basic select statement', () => {
    const got = Parser.parse([
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'a', type: 'name'},
      {value: ' ', type: 'whitespace'},
      {value: 'from', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'x', type: 'name'},
      {value: '', type: 'EOF'},
    ])
    const output = '<span class="kw select">select</span> <span class="name">a</span> <span class="kw from">from</span> <span class="name">x</span>'

    expect(got).toBe(output)
  })


  it('should parse numbers', () => {
    const got = Parser.parse([
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: '1', type: 'number'},
      {value: '', type: 'EOF'}
    ])
    const output = '<span class="kw select">select</span> <span class="number">1</span>'
    expect(got).toBe(output)
  })

  it('should parse should not parse one name', () => {
    const got = Parser.parse([
      {value: 'select', type: 'keyword'},
      {value: ' ', type: 'whitespace'},
      {value: 'a', type: 'name'},
      {value: '', type: 'EOF'}
    ])
    const output = '<span class="kw select">select</span> <span class="name">a</span>'
    expect(got).toBe(output)
  })
})


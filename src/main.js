import Tokenizer from './tokenizer'
import Parser from './parser'

const editor = document.getElementById('query')

editor.addEventListener('keyup', onKeyUp)

function onKeyUp(e) {
  if (e.ctrlKey) {
    console.log(e.keyCode)
    e.preventDefault()
    return
  }

  const stmt = editor.textContent
  const tokens = Tokenizer.tokenize(stmt)
  const newHtml = Parser.parse(tokens)

  const sel = window.getSelection()
  const range = sel.getRangeAt(0)
  range.setStart(editor, 0)
  const len = range.toString().length

  editor.innerHTML = newHtml

  {
    const pos = getTextNodeAtPosition(editor, len)
    sel.removeAllRanges()
    const range = new Range()
    range.setStart(pos.node, pos.position)
    sel.addRange(range)
  }
}

function getTextNodeAtPosition(root, index){
  const NODE_TYPE = NodeFilter.SHOW_TEXT;
  var treeWalker = document.createTreeWalker(root, NODE_TYPE, function next(elem) {
    if(index > elem.textContent.length){
      index -= elem.textContent.length;
      return NodeFilter.FILTER_REJECT
    }
    return NodeFilter.FILTER_ACCEPT;
  });
  var c = treeWalker.nextNode();
  return {
    node: c? c: root,
    position: index
  };
}


function MAll(ruleList) {
  return (_, tokens) =>
    ruleList.every(r => r(tokens))
}

function MToken(rule) {
  return (_, tokens) => {
  }
}

function MOne(ruleList) {
}

export default {
  MAll,
  MToken,
}

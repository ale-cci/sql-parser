import {parsingRules} from './rules'
import {keywords} from './keywords'


function parse(tokens) {
  const whitespaces = [
    'whitespace'
  ]

  const stmtTokens = tokens.filter(t => whitespaces.indexOf(t.type) === -1)
  const parsedTokens = parsingRules['file'](parsingRules, stmtTokens)

  if (parsedTokens !== stmtTokens.length) {
    const str = tokens.map(({value}) => value).join('')
    return `<span class="error">${str}</span>`
  }

  return tokens.map(({value, type}) => {
    if (keywords.indexOf(value.toUpperCase()) !== -1) {
      return `<span class="kw ${value.toLowerCase()}">${value}</span>`
    } else if (type !== 'whitespace' && type !== 'EOF') {
      return `<span class="${type}">${value}</span>`
    } else {
      return value
    }
  }).join('')
}

export default {parse}


const parseTokens = parse
export { parseTokens }
